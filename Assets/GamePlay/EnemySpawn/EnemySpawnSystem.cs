﻿using System;
using GamePlay.Characters;
using UnityEngine;
using Object = UnityEngine.Object;

namespace GamePlay.EnemySpawn
{
    public class EnemySpawnSystem
    {
        public event Action<Enemy> OnSpawn; 
        
        private PointSpawnProvider _pointSpawnProvider;
        private EnemyPrefabProvider _enemies;

        private Player _player;

        public EnemySpawnSystem(PointSpawnProvider pointSpawnProvider, EnemyPrefabProvider enemies, Player player)
        {
            _pointSpawnProvider = pointSpawnProvider;
            _enemies = enemies;

            _player = player;
        }
        
        public void SpawnEnemy()
        {
            var spawnPoint = _pointSpawnProvider.RandomSpawnPoint();
            var enemyPrefab = _enemies.RandomEnemy();
            var enemy = Object.Instantiate(enemyPrefab, spawnPoint.position, Quaternion.identity);
            
            enemy.GetComponent<Enemy>().SetTarget(_player.gameObject);
            
            OnSpawn?.Invoke(enemy);
        } 
    }
}