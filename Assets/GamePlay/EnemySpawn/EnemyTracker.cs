﻿using System.Collections.Generic;
using GamePlay.Characters;
using UnityEngine;

namespace GamePlay.EnemySpawn
{
    public class EnemyTracker
    {
        public int CountEnemies => _enemies.Count;
        
        private EnemySpawnSystem _enemySpawnSystem;
        private List<Character> _enemies = new List<Character>();
        
        private EnemyTracker(EnemySpawnSystem enemySpawnSystem)
        {
            _enemySpawnSystem = enemySpawnSystem;

            _enemySpawnSystem.OnSpawn += ActivateEnemy;
        }
        
        private void ActivateEnemy(Enemy enemy)
        {
            _enemies.Add(enemy);
            enemy.OnDeath += OnDestroyed;
        }

        private void OnDestroyed(Character enemy)
        {
            if (_enemies.Remove(enemy))
            {
                enemy.OnDeath -= OnDestroyed;
                Object.Destroy(enemy.gameObject);
            }
        }
    }
}