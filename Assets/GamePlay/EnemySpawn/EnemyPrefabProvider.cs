﻿using GamePlay.Characters;
using UnityEngine;

namespace GamePlay.EnemySpawn
{
    public class EnemyPrefabProvider : MonoBehaviour
    {
        [SerializeField]
        private Enemy[] monsterPrefabs;
        
        public Enemy RandomEnemy()
        {
            var index = Random.Range(0, monsterPrefabs.Length);
            return monsterPrefabs[index];
        }
    }
}