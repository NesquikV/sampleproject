﻿using UnityEngine;

namespace GamePlay.EnemySpawn
{
    public class PointSpawnProvider : MonoBehaviour
    {
        [SerializeField]
        private Transform[] spawnPoints;
        
        public Transform RandomSpawnPoint()
        {
            var index = Random.Range(0, spawnPoints.Length);
            return spawnPoints[index];
        }
    }
}