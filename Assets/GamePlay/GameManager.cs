﻿using System.Collections;
using GamePlay.Characters;
using GamePlay.EnemySpawn;
using UnityEngine;
using Zenject;

namespace GamePlay
{
    public class GameManager :  MonoBehaviour
    {
        [SerializeField]
        private int countEnemies = 10;

        [SerializeField]
        private float timeSpawnNewEnemy = 1f;
        
        private Player _player;
        private EnemyTracker _enemyTracker;
        private EnemySpawnSystem _enemySpawnSystem;

        [Inject]
        private void Construction(Player player, EnemyTracker enemyTracker, EnemySpawnSystem enemySpawnSystem)
        {
            _player = player;
            _enemyTracker = enemyTracker;
            _enemySpawnSystem = enemySpawnSystem;
        }
        
        private void Start()
        {
            _player.OnDeath += DeathPlayer;
            StartCoroutine(SpawnEnemy());
        }

        private void OnDestroy()
        {
            _player.OnDeath -= DeathPlayer;
        }
        
        private IEnumerator SpawnEnemy()
        {
            while (true)
            {
                yield return new WaitForSeconds(timeSpawnNewEnemy);
                
                if (_enemyTracker.CountEnemies < countEnemies)
                {
                    _enemySpawnSystem.SpawnEnemy();
                }
            }
        }

        private void DeathPlayer(Character player)
        {
            Destroy(_player.gameObject);
        }
    }
}