using UnityEngine;
using Zenject;

namespace GamePlay.Weapons
{
    public class Сannon : Weapon
    {
        [SerializeField]
        [Min(1)]
        private float speedProjectile = 1; 
        
        private Projectile _projectilePrefab;

        [Inject]
        private void Construction(Projectile projectilePrefab)
        {
            _projectilePrefab = projectilePrefab;
        }
        
        protected override void ConcreteShot()
        {
            var projectile = Instantiate(_projectilePrefab, pointShot.position, pointShot.rotation);
            projectile.Init(speedProjectile, damage);
        }
    }
}