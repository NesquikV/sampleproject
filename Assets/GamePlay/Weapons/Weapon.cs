using System.Collections;
using UnityEngine;

namespace GamePlay.Weapons
{
    public abstract class Weapon : MonoBehaviour
    {
        [SerializeField]
        protected Transform pointShot;
        
        [SerializeField]
        protected int damage;

        [SerializeField]
        private float cooldownTime = 1;
        
        private bool _cooldown;

        public bool IsCooldown()
        {
            return _cooldown;
        }
        
        public void Shot()
        {
            if (_cooldown)
            {
                return;
            }

            _cooldown = true;
            
            ConcreteShot();
            StartCoroutine(Cooldown());
        }

        protected abstract void ConcreteShot();
        
        private IEnumerator Cooldown()
        {
            yield return new WaitForSeconds(cooldownTime);
            _cooldown = false;
        }
    }
}