﻿using System.Collections;
using GamePlay.Characters;
using UnityEngine;

namespace GamePlay.Weapons
{
    public class Laser : Weapon
    {
        [SerializeField]
        private LineRenderer viewShot;
        
        protected override void ConcreteShot()
        {
            Debug.Log("shotLaser");
            var startPoint = pointShot.position;

            var hits = Physics.RaycastAll(startPoint, pointShot.TransformDirection(Vector3.forward), 
                Mathf.Infinity);

            foreach (var hit in hits)
            {

                if (hit.transform.gameObject.layer == LayerMask.NameToLayer("Obstruction"))
                {
                    viewShot.SetPositions(new []{startPoint, hit.point});
                    continue;
                }

                if (hit.transform.TryGetComponent<Enemy>(out var enemy))
                {
                    enemy.TakeDamage(damage);
                }
            }
                
            viewShot.enabled = true;
            StartCoroutine(HideLaser());
        }

        private IEnumerator HideLaser()
        {
            yield return new WaitForSeconds(.1f);
            viewShot.enabled = false;
        }
    }
}