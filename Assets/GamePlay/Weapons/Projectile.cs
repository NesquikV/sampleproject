using GamePlay.Characters;
using UnityEngine;

namespace GamePlay.Weapons
{
    public class Projectile : MonoBehaviour
    {
        private float _speed;
        private int _damage;

        public void Init(float speed, int damage)
        {
            _speed = speed;
            _damage = damage;
        }
        
        private void Update()
        {
            var vectorDirection = transform.rotation * new Vector3(0, 0, 1);
            transform.position += _speed * vectorDirection * Time.deltaTime;
        }

        private void OnCollisionEnter(Collision other)
        {
            if (other.gameObject.TryGetComponent<Enemy>(out var enemy))
            {
                enemy.TakeDamage(_damage);
            }

            Destroy(gameObject);
        }
    }
}