﻿using System;
using UnityEngine;

namespace GamePlay.Characters
{
    public class Character : MonoBehaviour
    {
        public event Action<Character> OnDeath;
        
        [SerializeField]
        private float health;
        
        [SerializeField]
        [Range(0.1f, 1)]
        private float shield;

        public void TakeDamage(int damage)
        {
            health -= damage * shield;

            if (health <= 0)
            {
                OnDeath?.Invoke(this);
            }
        }
    }
}