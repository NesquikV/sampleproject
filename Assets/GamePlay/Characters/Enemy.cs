﻿using System.Collections;
using UnityEngine;

namespace GamePlay.Characters
{
    public class Enemy : Character
    {
        [SerializeField]
        [Min(1)]
        private float speedMove;
        
        [SerializeField]
        [Min(1)]
        private int damage;
        
        [SerializeField]
        private float cooldownTime = 1;
        
        private GameObject _target;
        private bool _cooldownAttack;

        private void Update()
        {
            if (!_target)
            {
                Move(0);
                return;
            }
                
            var distance = _target.transform.position - transform.position;
            var direction = Quaternion.LookRotation(distance, Vector3.up);

            if (distance.magnitude > 1)
            {
                Move(1);
                transform.rotation = direction;
            }
            else if (!_cooldownAttack)
            {
                _target.GetComponent<Player>().TakeDamage(damage);
                _cooldownAttack = true;
                StartCoroutine(Cooldown());
            }
        }

        public void SetTarget(GameObject target)
        {
            _target = target;
        }
        
        private void Move(int direction)
        {
            var vectorDirection = transform.rotation * new Vector3(0, 0, direction);
            transform.position += speedMove * vectorDirection * Time.deltaTime;
        }
        
        private IEnumerator Cooldown()
        {
            yield return new WaitForSeconds(cooldownTime);
            _cooldownAttack = false;
        }
    }
}