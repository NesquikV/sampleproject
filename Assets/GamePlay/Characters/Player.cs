using System.Collections.Generic;
using GamePlay.Helpers;
using GamePlay.Weapons;
using UnityEngine;
using Zenject;
using Vector3 = UnityEngine.Vector3;

namespace GamePlay.Characters
{
    public class Player : Character
    {
        [SerializeField]
        [Min(1)]
        private float speedMove;

        [SerializeField]
        [Min(1)]
        private float speedRotate;

        [SerializeField]
        private List<Weapon> weapons;

        [Inject(Id = Markers.BoundaryWorld)]
        private BoxCollider _boundaryWorld;
        
        private int _indexWeapon;
        
        private void Start()
        {
            EnableWeapon();
        }

        public void Move(int direction)
        {
            var vectorDirection = transform.rotation * new Vector3(0, 0, direction);
            transform.position += speedMove * vectorDirection * Time.deltaTime;

            if (!_boundaryWorld.bounds.Contains(transform.position))
            {
                transform.position = _boundaryWorld.ClosestPoint(transform.position);
            }
        }

        public void Rotate(int direction)
        {
            transform.Rotate(Vector3.up * direction * speedRotate * Time.deltaTime);
        }

        public void Shot()
        {
            var currentWeapon = weapons[_indexWeapon];
            if (currentWeapon)
            {
                currentWeapon.Shot();
            }
        }

        public void ChangeWeapon(int direction)
        {
            if (weapons[_indexWeapon].IsCooldown())
            {
                return;
            }
            
            _indexWeapon += direction;
            
            if (_indexWeapon < 0)
            {
                _indexWeapon = weapons.Count - 1;
            }

            if (_indexWeapon > weapons.Count - 1)
            {
                _indexWeapon = 0;
            }

            EnableWeapon();
        }

        private void EnableWeapon()
        {
            DisableAllWeapons();
            weapons[_indexWeapon].gameObject.SetActive(true);
        }
        
        private void DisableAllWeapons()
        {
            foreach (var weapon in weapons)
            {
                weapon.gameObject.SetActive(false);
            }
        }
    }
}