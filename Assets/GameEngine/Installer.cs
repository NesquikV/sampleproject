using System;
using GamePlay.Characters;
using GamePlay.EnemySpawn;
using GamePlay.Helpers;
using GamePlay.Weapons;
using UnityEngine;
using Zenject;

namespace GameEngine
{
    public class Installer : MonoInstaller
    {
        [SerializeField]
        private Player player;

        [SerializeField] 
        private BoxCollider boundaryWorld;
        
        [SerializeField]
        private Projectile projectilePrefab;
        
        [SerializeField]
        private EnemyPrefabProvider enemyPrefabProvider;

        [SerializeField]
        private PointSpawnProvider pointSpawnProvider;
        
        public override void InstallBindings()
        {
            Container.BindInstance(player).AsSingle();
            
            Container.BindInstance(projectilePrefab).AsSingle();
            Container.BindInstance(enemyPrefabProvider).AsSingle();
            Container.BindInstance(pointSpawnProvider).AsSingle();

            Container.Bind<BoxCollider>().WithId(Markers.BoundaryWorld).FromInstance(boundaryWorld).AsSingle();
            
            Container.Bind<EnemySpawnSystem>().AsSingle();
            Container.Bind<EnemyTracker>().AsSingle();
            
            Container.Bind(typeof(ITickable), typeof(InputKeyboard)).To<InputKeyboard>().AsSingle();
            Container.Bind(typeof(IInitializable), typeof(IDisposable), typeof(InputController))
                .To<InputController>().AsTransient();
        }
    }
}