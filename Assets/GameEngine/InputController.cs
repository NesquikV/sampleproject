using System;
using GamePlay.Characters;
using Zenject;

namespace GameEngine
{
    public class InputController : IInitializable, IDisposable
    {
        private readonly InputKeyboard _inputKeyboard;
        private readonly Player _player;
        
        public InputController(InputKeyboard inputKeyboardSystem, Player player)
        {
            _inputKeyboard = inputKeyboardSystem;
            _player = player;
        }

        public void Initialize()
        {
            _inputKeyboard.OnMove += _player.Move;
            _inputKeyboard.OnRotate += _player.Rotate;
            _inputKeyboard.OnShot += _player.Shot;
            _inputKeyboard.OnChangeWeapon += _player.ChangeWeapon;
        }
        
        public void Dispose()
        {
            _inputKeyboard.OnMove -= _player.Move;
            _inputKeyboard.OnRotate -= _player.Rotate;
            _inputKeyboard.OnShot -= _player.Shot;
            _inputKeyboard.OnChangeWeapon -= _player.ChangeWeapon;
        }
    }
}