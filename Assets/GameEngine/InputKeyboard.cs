﻿using System;
using UnityEngine;
using Zenject;

namespace GameEngine
{
    public class InputKeyboard : ITickable
    {
        public event Action<int> OnMove;
        public event Action<int> OnRotate;
        public event Action OnShot;
        public event Action<int> OnChangeWeapon;
        
        public void Tick()
        {
            Move();
            Rotate();
            Shot();
            SwitchWeapon();
        }
        
        private void Move()
        {
            if (Input.GetKey(KeyCode.UpArrow))
            {
                OnMove?.Invoke(1);
            }
            else if (Input.GetKey(KeyCode.DownArrow))
            {
                OnMove?.Invoke(-1);
            }
        }

        private void Rotate()
        {
            if (Input.GetKey(KeyCode.LeftArrow))
            {
                OnRotate?.Invoke(-1);
            }
            else if (Input.GetKey(KeyCode.RightArrow))
            {
                OnRotate?.Invoke(1);
            }
        }

        private void Shot()
        {
            if (Input.GetKey(KeyCode.X))
            {
                OnShot?.Invoke();
            }
        }

        private void SwitchWeapon()
        {
            if (Input.GetKeyUp(KeyCode.Q))
            {
                OnChangeWeapon?.Invoke(-1);
            }
            else if (Input.GetKeyUp(KeyCode.W))
            {
                OnChangeWeapon?.Invoke(1);
            }
        }
    }
}